#!/bin/bash
set -euo pipefail

while read -r id; do
    terraform taint -module="${1}" "$(cut -d. -f3- <<<"$id")" || true
done < <(terraform state list | grep "^module\\.${1}\\.")
