#!/bin/bash
set -euo pipefail

readonly GITLAB_RUNNER_VERSION=${GITLAB_RUNNER_VERSION:-10.1.0}
readonly GITLAB_RUNNER_IMAGE=${GITLAB_RUNNER_IMAGE:-"gitlab/gitlab-runner:alpine-v${GITLAB_RUNNER_VERSION}"}
readonly MINIO_SERVER_VERSION=${MINIO_SERVER_VERSION:-RELEASE.2018-03-16T22-52-12Z}
readonly MINIO_CLIENT_VERSION=${MINIO_CLIENT_VERSION:-RELEASE.2018-03-14T23-16-27Z}
readonly MINIO_SERVER_IMAGE=${MINIO_SERVER_IMAGE:-"minio/minio:${MINIO_SERVER_VERSION}"}
readonly MINIO_CLIENT_IMAGE=${MINIO_CLIENT_IMAGE:-"minio/mc:${MINIO_CLIENT_VERSION}"}

readonly MINIO_ACCESS_KEY=${MINIO_ACCESS_KEY:-gitlab}
readonly MINIO_SECRET_KEY=${MINIO_SECRET_KEY:-runnercache}

start_cache() {
    echo "Starting cache server..."

    docker network create gitlab-ci > /dev/null || true

    if docker inspect gitlab-ci-cache > /dev/null; then
        docker start gitlab-ci-cache
    else
        docker run \
            --detach \
            --env "MINIO_ACCESS_KEY=${MINIO_ACCESS_KEY}" \
            --env "MINIO_SECRET_KEY=${MINIO_SECRET_KEY}" \
            --volume gitlab-ci-cache:/data \
            --name gitlab-ci-cache \
            "${MINIO_SERVER_IMAGE}" \
            server \
            /data

        docker network connect gitlab-ci gitlab-ci-cache
    fi

    >&2 echo "Waiting for cache server..."

    while ! docker exec gitlab-ci-cache nc -z -w 60 127.0.0.1 9000; do
        sleep 1
    done

    >&2 echo "Creating cache bucket..."

    docker run \
        --rm \
        --tty \
        --network gitlab-ci \
        --env "MINIO_ACCESS_KEY=${MINIO_ACCESS_KEY}" \
        --env "MINIO_SECRET_KEY=${MINIO_SECRET_KEY}" \
        --entrypoint sh \
        "$MINIO_CLIENT_IMAGE" \
            -c \
            '
                mc config host add minio http://gitlab-ci-cache:9000 "$MINIO_ACCESS_KEY" "$MINIO_SECRET_KEY"
                mc mb minio/gitlab-ci || true
            '

    >&2 echo "Cache server setup."
}

drop_cache() {
    >&2 echo "Deleting cache server..."

    docker rm --force --volumes gitlab-ci-cache > /dev/null || true
    docker volume rm gitlab-ci-cache || true

    >&2 echo "Cache server deleted."
}

main() {
    if [ "${1:-}" = "--help" ]; then
        shift

        >&2 echo "$(basename "$0") [--cache] [--gitlab-runner-argument ...] <job name>"
        >&2 echo "$(basename "$0") --drop-cache"

        docker run --rm -it "$GITLAB_RUNNER_IMAGE" exec docker --help
        return
    fi

    if [ "${1:-}" = "--drop-cache" ]; then
        shift;
        drop_cache
        return
    fi

    local docker_arguments=(
    )
    local runner_arguments=(
        '--docker-pull-policy=if-not-present' # if-not-present
    )

    if [ "${1:-}" = "--cache" ]; then
        shift

        start_cache

        docker_arguments+=(
            --network default
            --network gitlab-ci
        )

        runner_arguments+=(
            --docker-links gitlab-ci-cache

            --cache-type s3
            --cache-s3-server-address gitlab-ci-cache:9000
            --cache-s3-access-key "${MINIO_ACCESS_KEY}"
            --cache-s3-secret-key "${MINIO_SECRET_KEY}"
            --cache-s3-bucket-name gitlab-ci
            --cache-s3-insecure
            --cache-cache-shared
        )
    fi

    # set -x
    docker run --rm -it \
        --volume /var/run/docker.sock:/var/run/docker.sock \
        --volume "$(pwd):$(pwd)" \
        --workdir "$(pwd)" \
        "${docker_arguments[@]+"${docker_arguments[@]}"}" \
        "$GITLAB_RUNNER_IMAGE" \
            exec \
            docker \
            "${runner_arguments[@]+"${runner_arguments[@]}"}" \
            "$@"
}

main "$@"
